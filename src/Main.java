import javax.sound.sampled.*;
import  java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static javax.sound.sampled.AudioSystem.*;
//main method
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        //start scanner
        Scanner sc = new Scanner(System.in);
        //create a new linked list
        LinkedList song = null;
        System.out.println("Choose the wished options");
        //the posible notes, if you want add one just add it on this array, the . is the silent
        String[] notes = {"Do","Re","Mi","Fa","Sol","La","Si","do_alto","."};
        //create the url of the audio files
        String[] urls = new String[9];
        for(int l=0;l<notes.length;l++){
            urls[l] =((notes[l].toLowerCase())+".wav");
        }
        //keep the execution
        boolean keepgoing = true;
        //the size
        int s = 0;
        while(keepgoing) {
            System.out.println("1. Define the size" + "\n" + "2. Add notes"  + "\n" +"3. Add a note" + "\n" + "4. Delete a note" + "\n" + "5. lisent the song" + "\n" + "6. go out");
            //selected option
            String o = sc.next();
            int o2=15;
            try{
               o2 =Integer.parseInt(o);
            }catch (Exception e ){
                System.out.println("This is not a number");
            }
            switch (o2) {
                case 1:
                    //put the size of the array and start the linked list
                    System.out.println("Put the size of the list:");
                    s = sc.nextInt();
                    song = new LinkedList(0);
                    break;
                case 2:
                    //full the array with the numbers of the array
                    for (int i = 0; i < s; i++) {
                        System.out.println("Put the value of the position " + (i + 1) + " :");
                        System.out.println(Arrays.toString(notes));
                        String op = sc.next();
                        boolean insert = true;
                        for (int j = 0; j < notes.length; j++) {
                            if (notes[j].toLowerCase().equals(op.toLowerCase())) {
                                song.append(j);
                                song.print(notes);
                                insert = false;
                            }
                        }
                        if (insert) {
                            System.out.println("insert a correct data");
                            i--;
                        }
                    }
                    //print the list
                    song.print(notes);
                    //if the size is 0
                    if (s == 0) {
                        System.out.println("you had not defined the size of the list");
                    }
                    break;
                case 3:
                    //add a note in a selected position
                    System.out.println("Insert the index where add the note:");
                    //recive the index
                    int i = sc.nextInt();
                    i--;
                    System.out.print("Insert the data to add: ");
                    System.out.println(Arrays.toString(notes));
                    String op = sc.next();
                    if (i<s) {
                        //if the index exist
                        for (int j = 0; j < notes.length; j++) {
                            if (notes[j].toLowerCase().equals(op.toLowerCase())) {
                                song.insert(i, j);
                                s++;
                            }
                        }
                    }else{
                        //if the index dont exist add the data to the tail
                        for (int j = 0; j < notes.length; j++) {
                            if (notes[j].toLowerCase().equals(op.toLowerCase())) {
                                song.append(j);
                                s++;
                            }
                        }
                    }
                    song.print(notes);
                    break;
                case 4:
                    //remove a note
                    System.out.println("Insert the index where remove the note: ");
                    int w = sc.nextInt();
                    w--;
                    song.remove(w);
                    song.print(notes);
                    //reduce the size
                    s--;
                    break;
                case 5:
                    //play the song
                    if(s >0) {
                        song.print(notes);
                        for (int u = 1; u <= s; u++) {
                            playSound(urls[song.get(u)]);
                        }
                    }
                    break;
                case 6:
                    //ends the program
                    keepgoing=false;
                    break;
                default:
                    //other option
                    System.out.println("this is not a valid option");
                    break;
            }
        }

    }
    //method to play a song
    public static void playSound(final String url) throws InterruptedException {
        // The wrapper thread is unnecessary, unless it blocks on the
        // Clip finishing; see comments.
        if (!(url.equals("..wav"))) {
            //if it is a sound
            try {
                Clip clip = getClip();
                //open the file
                AudioInputStream inputStream = getAudioInputStream(
                        Main.class.getResourceAsStream("/sounds/" + url));
                //get the type of the audio format (especial to work on linux)
                DataLine.Info info = new DataLine.Info(Clip.class, inputStream.getFormat());
                //open the file in a clip
                clip = (Clip)AudioSystem.getLine(info);
                //put the file
                clip.open(inputStream);
                //clip.loop(Clip.LOOP_CONTINUOUSLY);
                //reproduce the sound
                clip.start();
                //wait to lisend the sound, if it is more short the song is more fast
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }else{
            //if it is a silent
            TimeUnit.MILLISECONDS.sleep(200);
        }
}}
